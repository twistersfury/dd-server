# D&D Server Map #
This is a relatively simple NodeJS server for the purpose of playing Dungeons and Dragons. It uses socket.io, AngularJS, and
express to generate a grid area that allows users to build a map. There is also functionality to chat, player names,
and roll dice.

# Installation/Setup #

```
npm install
./node_modules/grunt-cli/bin/grunt run
node ./app/index.js
```

Navigate to http://localhost:3000 in a couple browsers.