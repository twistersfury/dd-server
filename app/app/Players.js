/**
 * Created by fenikkusu on 1/22/15.
 */
;(function() {
    var Player = require('./Player.js');

    function Players(oServer) {
        var tThis = this;

        tThis._oServer = oServer;

        tThis.init();
    }

    Players.prototype.init = function() {
        var tThis = this;

        tThis._aPlayers = {};
    };

    Players.prototype.playerJoin = function(oClient) {
        var tThis = this;

        tThis._aPlayers[oClient.id] = new Player(oClient, tThis);
        tThis._oServer.getMap().send();

        tThis._oServer.sendData('message', 'New User Connected');
    };

    Players.prototype.sendData = function(oData) {
        this._oServer.sendData(oData);
    };

    Players.prototype.getServer = function() {
        return this._oServer;
    };

    module.exports = Players;
})();