/**
 * Created by fenikkusu on 1/23/15.
 */
;(function() {
    "use strict";

    function GridSquare(oMap, iRow, iColumn) {
        var tThis = this;

        tThis._oMap    = oMap;

        tThis._oSquare = {
            row    : iRow,
            column : iColumn,
            status : false,
            color  : 0
        };
    }

    GridSquare.prototype.update = function(oSquare) {
        var tThis = this;

        tThis._oSquare = oSquare;
        tThis.send();
    };

    GridSquare.prototype.getData = function() {
        return this._oSquare;
    };

    GridSquare.prototype.send = function(bReturn) {
        var tThis = this;

        tThis._oMap.getServer().sendData(
            'gridSquare',
            tThis._oSquare
        );
    };

    GridSquare.prototype.setActive = function(bActive) {
        this._oSquare.status = bActive;
    };

    module.exports = GridSquare;
})();