/**
 * Created by fenikkusu on 1/23/15.
 */
;(function() {
    var GridSquare = require(__dirname +'/GridSquare.js');

    function Map(oServer) {
        var tThis = this;

        tThis._oServer  = oServer;
        tThis._oGrid    = {
            size    : 30,
            squares : {}
        };

        tThis._init();
    }

    Map.prototype._init = function() {
        var tThis = this;

        for(var iRow = 0; iRow < tThis._oGrid.size; iRow++) {
            for(var iColumn = 0; iColumn < tThis._oGrid.size; iColumn++) {
                tThis._oGrid.squares[iRow + '-' + iColumn] = new GridSquare(tThis, iRow, iColumn);
            }
        }
    };

    Map.prototype.send = function() {
        var tThis = this,
            oSend = {
                size    : tThis._oGrid.size,
                squares : []
            };

        for (var sKey in tThis._oGrid.squares) {
            if (tThis._oGrid.squares.hasOwnProperty(sKey)) {
                oSend.squares.push(tThis._oGrid.squares[sKey].getData());
            }
        }

        tThis._oServer.sendData('grid', oSend);
    };

    Map.prototype.updateSquare = function(oSquare) {
        var sKey = oSquare.row + '-' + oSquare.column;
        if ('undefined' !== (typeof this._oGrid.squares[sKey])) {
            return this._oGrid.squares[sKey].update(oSquare);
        } else {
            return {
                'error' : 'Square Does Not Exist'
            };
        }
    };

    Map.prototype.eraseMap = function() {
        for(var sKey in this._oGrid.squares) {
            if (this._oGrid.squares.hasOwnProperty(sKey)) {
                this._oGrid.squares[sKey].setActive(false);
            }
        }

        this.send();
    };

    Map.prototype.getServer = function() {
        return this._oServer;
    };

    module.exports = Map;
})();