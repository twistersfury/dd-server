/**
 * Created by fenikkusu on 1/22/15.
 */
;(function() {
    "use strict";
    function Player(oSocket, oPlayers) {
        var tThis = this;

        tThis._oSocket  = oSocket;
        tThis._oPlayers = oPlayers;
        tThis._oData = {
            name : 'Player'
        };

        tThis._init();
    }

    Player.prototype.getName = function() {
        return this._oData.name;
    };

    Player.prototype._init = function() {
        var tThis = this;

        tThis._oSocket.on(
            'playerData',
            function(oData) {
                tThis.updateData(oData);
            }
        );

        tThis._oSocket.on(
            'gridData',
            function(oSquare) {
                tThis._oPlayers.getServer().getMap().updateSquare(oSquare);
            }
        );

        tThis._oSocket.on(
            'gridErase',
            function() {
                tThis._oPlayers.getServer().getMap().eraseMap();
            }
        );

        tThis._oSocket.on(
            'dice',
            function(iSides) {
                tThis._oPlayers.getServer().rollDice(iSides, tThis);
            }
        );

        tThis._oSocket.on(
            'message',
            function(sMessage) {
                tThis._oPlayers.getServer().sendData('message', sMessage, tThis);
            }
        );
    };

    Player.prototype.updateData = function(oData) {
        if (oData.name !== this._oData.name) {
            this._oPlayers.getServer().sendData('message', this._oData.name + ' is now known as ' + oData.name);
        }

        this._oData = oData;
    };

    module.exports = Player;
})();