/**
 * Created by fenikkusu on 1/22/15.
 */
;(function() {
    "use strict";

    var Players = require(__dirname + '/Players.js'),
        Map     = require(__dirname + '/Map.js');

    function DndServer(oConnection, oConfig) {
        var tThis = this;

        tThis._oConnection = oConnection;

        tThis._oPlayers    = new Players(tThis);
        tThis._oMap        = new Map(tThis);

        tThis.init();
    }

    DndServer.prototype.init = function() {
        var tThis = this;

        tThis._oConnection.on('connection', function(oClient) {
            console.log('A User Connected: ' + oClient.id);
            tThis._oPlayers.playerJoin(oClient);
        });
    };

    DndServer.prototype.sendData = function(sType, oData, oPlayer) {
        var oPacket = {
            data   : oData,
            player : ('undefined' !== (typeof oPlayer)) ? oPlayer.getName() : null
        };

        this._oConnection.emit(sType, oPacket);
    };

    DndServer.prototype.rollDice = function(iSides, oPlayer) {
        var iRoll = Math.round((Math.random() * iSides) + 1, 0);
        if (iRoll > iSides) {
            iRoll = iSides;
        }

        this.sendData('message', oPlayer.getName() + ' rolled a D' + iSides + ' and got a ' + iRoll);
    };

    DndServer.prototype.getMap = function() {
        return this._oMap;
    };

    module.exports = DndServer;
})();