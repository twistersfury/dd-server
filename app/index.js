/**
 * Created by fenikkusu on 1/20/15.
 */
;(function() {
    var expressServer  = require('express'),
        expressApp     = expressServer(),
        httpInstance   = require('http').Server(expressApp),
        masterSocket   = require('socket.io')(httpInstance),
        serverInstance = new (require('./app/DndServer.js'))(masterSocket);


    expressApp.use(
        '/assets',
        expressServer.static(
            __dirname + '/assets'
        )
    );

    expressApp.get(
        '/',
        function(oRequest, oResponse) {
            oResponse.sendFile(
                __dirname + '/assets/templates/index.html'
            );
        }
    );

    httpInstance.listen(
        3000,
        function() {
            console.log('Server Started On Port 3000');
        }
    );
})();