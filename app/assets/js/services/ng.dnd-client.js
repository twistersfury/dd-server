/**
 * Created by fenikkusu on 1/28/15.
 */
;(function() {
    var DndClient = function($timeout) {
        this._$timeout   = $timeout;
        this._aCallbacks = {};

        this._oPlayer    = {
            name : 'Player'
        };

        this._init();
    };

    DndClient.prototype.getPlayer = function() {
        return this._oPlayer;
    };

    DndClient.prototype._init = function() {
        var tThis = this;

        tThis._oSocket = io();
        tThis._oSocket._onEvent = tThis._oSocket.onevent;
        tThis._oSocket.onevent = function(oPacket) {
            tThis._oSocket._onEvent(oPacket);
            tThis.broadcast(oPacket.data[0], oPacket.data[1]);
        };

        tThis.on('*').then(function(vData) {
            console.log(vData.type, vData.data);
        });
    };

    DndClient.prototype.defer = function() {
        var aCallbacks = [];
        return {
            then : function(fnCallback) {
                aCallbacks.push(fnCallback);
            },
            broadcast : function(vData) {
                angular.forEach(aCallbacks, function(fnCallback) {
                    var tData = fnCallback.call(this, vData);
                    if (tData !== null) {
                        vData = tData;
                    }
                });
            }
        };
    };

    DndClient.prototype.on = function(sIdent) {
        if ('undefined' === (typeof this._aCallbacks[sIdent])) {
            this._aCallbacks[sIdent] = [];
        }

        var oDefer = this.defer();
        this._aCallbacks[sIdent].push(oDefer);

        return oDefer;
    };

    DndClient.prototype.broadcast = function(sIdent, vData) {
        var tThis = this;
        if (sIdent !== '*') {
            tThis.broadcast(
                '*',
                {
                    data : vData,
                    type : sIdent
                }
            );
        }

        if ('undefined' === (typeof this._aCallbacks[sIdent])) {
            return;
        }

        this._$timeout(function() {
            console.log('broadcast', sIdent);
            angular.forEach(tThis._aCallbacks[sIdent], function (oDefer) {
                oDefer.broadcast(vData);
            });
        }, 0);
    };

    DndClient.prototype.send = function(sType, vData) {
        this._oSocket.emit(sType, vData);
    };

    DndClient.prototype.setPlayer = function(oPlayer) {
        this.send('playerData', oPlayer);
        this._oPlayer = oPlayer;
    };

    angular.module('tf.dnd').service(
        'tf.dnd.client',
        [
            '$timeout',
            DndClient
        ]
    );
})();