/**
 * Created by fenikkusu on 1/20/15.
 */
;(function () {
    var ng = angular.module('tf.dnd.services');

    ng.service(
        'tf.dnd.services.socket',
        [
            '$rootScope',
            function($rootScope) {
                var _oSocket  = null,
                    _$emit    = null,
                    _oMethods = {
                        init        : function () {
                            _oSocket = io();

//                            _$emit = _oSocket.$emit;
//
//                            _oSocket.$emit = function () {
//                                var aArgs = Array.prototype.slice.call(arguments);
//
//                                console.log('here', aArgs);
//
//                                $rootScope.$emit('socket-'.aArgs[0], aArgs.slice(1));
//                                _$emit.apply(_oSocket, arguments);
//                            };

                            _oSocket.on('gridSquareData', function(oData) {
                                $rootScope.$emit('grid-square-data', oData);
                            });

                            _oSocket.on(
                                'messageData',
                                function (sMessage) {
                                    console.log('message', sMessage);
                                    $rootScope.$emit('message', sMessage);
                                }
                            );

                            _oSocket.on('gridData', function(oData) {
                                $rootScope.$emit('grid-data', oData);
                            });

                            _oSocket.on('connecting', function() {
                                _oMethods.isConnected = false;
                            });

                            _oSocket.on('connect', function() {
                                _oMethods.isConnected = true;
                            });

                            _oSocket.on('disconnect', function() {
                                _oMethods.isConnected = false;
                            });

                            _oSocket.on('reconnecting', function() {
                                _oMethods.isConnected = true;
                            });
                        },
                        isConnected : false,
                        sendData : function(oData) {
                            _oSocket.emit('gridData', oData);
                        },
                        sendPacket : function(sType, oData) {
                            _oSocket.emit(sType, oData);
                        }
                    };

                _oMethods.init();

                return _oMethods;
            }
        ]
    );
})();