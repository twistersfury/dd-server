/**
 * Created by fenikkusu on 1/20/15.
 */
;(function() {
    angular.module('tf.dnd.services', []);
    angular.module('tf.dnd.directives', ['tf.dnd.services', 'ngDialog']);

    var ng = angular.module(
        'tf.dnd',
        [
            'tf.dnd.services',
            'tf.dnd.directives'
        ]
    );
})();