/**
 * Created by fenikkusu on 1/21/15.
 */
;(function() {
    "use strict";
    var ng = angular.module('tf.dnd.directives');

    ng.directive(
        'dndGrid',
        [
            '$compile',
            '$timeout',
            '$window',
            '$rootScope',
            'tf.dnd.client',
            function($compile, $timeout, $window, $rootScope, svClient) {
                return {
                    scope : {
                        size     : '=',
                        zoom     : '=',
                        zoomSize : '='
                    },
                    templateUrl : '/assets/templates/tpl.grid.html',
                    replace: true,
                    controller : [
                        '$scope',
                        function($scope) {
                            $scope.data = {
                                columns    : [],
                                rows       : [],
                                mouseDown  : false,
                                mouseColor : 0
                            };

                            for (var iLoop = 0; iLoop < $scope.size; iLoop++) {
                                $scope.data.columns.push(iLoop);
                                $scope.data.rows.push(iLoop);
                            }
                        }
                    ],
                    link : function(oScope, oElement) {
                        function updateSquare(oOptions) {
                            if ('undefined' === (typeof oOptions.row) || 'undefined' === (typeof oOptions.column)) {
                                return;
                            }

                            var oSquare = $(".grid-column[row='" + oOptions.row + "'][column='" + oOptions.column + "']");

                            if (oOptions.status) {
                                oSquare.addClass('active');
                                oSquare.attr('color', oOptions.color);
                                oSquare.addClass('color-' + oOptions.color);
                            } else {
                                oSquare.removeClass('active');
                            }
                        }

                        function resizeBoxes() {
                            var iSize = oScope.zoomSize * (oScope.zoom / 100);

                            oElement.find('.grid-row').height(iSize);

                            oElement.find('.grid-column').width(iSize);
                            oElement.find('.grid-column').height(iSize);
                        }

                        angular.element($window).on('resize', function() {
                            resizeBoxes();
                        });

                        $timeout(function() {
                            resizeBoxes();
                        }, 0);

                        svClient.on('grid').then(
                            function(oData) {
                                angular.forEach(
                                    oData.data.squares,
                                    function(oItem) {
                                        updateSquare(oItem);
                                    }
                                );
                            }
                        );

                        svClient.on('gridSquare').then(
                            function(oData) {
                                updateSquare(oData.data);
                            }
                        );

//                        var squareOn = function() {
//                            $(this).removeClass('color-' + $(this).attr('color'));
//
//                            var iColor = parseInt($(this).attr('color')) + 1;
//                            if (iColor > 9) {
//                                iColor = 0;
//                            }
//
//                            $(this).attr('color', iColor);
//
//                            $(this).attr('color-' + $(this).attr('color'));
//
//                            $(this).addClass('active');
//
//
//                            svClient.send(
//                                'gridData',
//                                {
//                                    row : $(this).attr('row'),
//                                    column : $(this).attr('column'),
//                                    status : $(this).hasClass('active'),
//                                    color  : $(this).attr('color')
//                                }
//                            );
//                        };

                        var onMouseDown = function(oEvent) {
                            oScope.data.mouseDown = true;

                            $(this).removeClass('color-' + $(this).attr('color'));

                            var iColor = parseInt($(this).attr('color')) + 1;
                            if (iColor > 9) {
                                iColor = 0;
                            }

                            oScope.data.mouseColor = iColor;

                            onMouseOver.call(this, oEvent);
                        };

                        var onMouseOver = function(oEvent) {
                            if (oScope.data.mouseDown) {
                                $(this).removeClass('color-' + $(this).attr('color'));
                                $(this).addClass('color-' + oScope.data.mouseColor);
                                $(this).attr('color', oScope.data.mouseColor);
                                $(this).addClass('active');

                                svClient.send(
                                    'gridData',
                                    {
                                        row    : $(this).attr('row'),
                                        column : $(this).attr('column'),
                                        status : $(this).hasClass('active'),
                                        color  : $(this).attr('color')
                                    }
                                );
                            }
                        };

                        var onMouseUp = function(oEvent) {
                            oScope.data.mouseDown = false;
                        };

                        var squareOff = function(oEvent) {
                            oEvent.preventDefault();
                            oEvent.stopPropagation();

                            $(this).removeClass('active');
                            $(this).removeClass('color-' + $(this).attr('color'));

                            $(this).attr('color', 0);

                            svClient.send(
                                'gridData',
                                {
                                    row    : $(this).attr('row'),
                                    column : $(this).attr('column'),
                                    status : $(this).hasClass('active'),
                                    color  : $(this).attr('color')
                                }
                            );
                        };

                        for(var iRows = 0; iRows < oScope.size; iRows++) {
                            var oRow = angular.element('<div></div>');

                            oRow.addClass('grid-row');

                            for (var iColumns = 0; iColumns < oScope.size; iColumns++) {
                                var oSquare = angular.element('<div></div>');

                                oSquare.attr('column', iColumns);
                                oSquare.attr('row', iRows);
                                oSquare.attr('color', 0);

                                oSquare.addClass('grid-column');

                                oSquare.on('mousedown'  , onMouseDown);
                                oSquare.on('mouseup'    , onMouseUp);
                                oSquare.on('mouseover'  , onMouseOver);
                                oSquare.on('contextmenu', squareOff);

                                $compile(oSquare)(oScope);

                                oRow.append(oSquare);
                            }

                            oRow.append(angular.element('<div></div>').addClass('clear'));

                            oElement.append(oRow);
                        }
                    }
                };
            }
        ]
    );
})();