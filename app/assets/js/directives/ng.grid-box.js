/**
 * Created by fenikkusu on 1/21/15.
 */
;(function() {
    var ng = angular.module('tf.dnd.directives');

    ng.directive(
        'dndGridBox',
        [
            '$rootScope',
            function($rootScope) {
                return {
                    link : function(scope, element, oAttrs) {
                        $rootScope.$on(
                            'socket-data-' + oAttrs.square,
                            function(oOptions) {
                                console.log('grid-box');
                                var oSquare = $(".grid-square[row='" + oOptions.square + "']");

                                if (oOptions.status) {
                                    oSquare.removeClass('active');
                                } else {
                                    oSquare.addClass('active');
                                }
                            }
                        );
                    }
                }
            }
        ]
    )
})();