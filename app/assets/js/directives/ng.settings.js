/**
 * Created by Fenikkusu on 3/14/15.
 */
;(function() {
    "use strict";

    var ng = angular.module('tf.dnd.directives');

    ng.directive(
        'tfSettings',
        [
            function() {
                return {
                    replace  : true,
                    template : '<button ng-click="openSettings()" class="tf-settings glyphicon glyphicon-cog form-control btn btn-info"></button>',
                    controller : [
                        '$scope',
                        'ngDialog',
                        'tf.dnd.client',
                        function($scope, svDialog, svClient) {
                            $scope.data = {
                                player : null
                            };

                            $scope.openSettings = function() {
                                $scope.data.player = angular.extend({}, svClient.getPlayer());

                                svDialog.open(
                                    {
                                        scope: $scope,
                                        template: '/assets/templates/tpl.settings.html'
                                    }
                                ).closePromise.then(
                                    function(oDialog) {
                                        if (oDialog.value === false) {
                                            return;
                                        }

                                        svClient.setPlayer($scope.data.player);
                                    }
                                );
                            };
                        }
                    ]
                };
            }
        ]
    );
})();