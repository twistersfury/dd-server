/**
 * Created by Fenikkusu on 3/15/15.
 */
;(function() {
    "use strict";
    var ng = angular.module('tf.dnd.directives');

    ng.directive(
        'tfGridErase',
        [
            'tf.dnd.client',
            function(svClient) {
                return {
                    template : '<button style="position: absolute; top: 35px; right: 0; margin: 3px; height: 30px; width:30px;" class="glyphicon glyphicon-erase btn btn-warning" ng-click="eraseMap()"></button>',
                    replace : true,
                    link : function(oScope) {
                        oScope.eraseMap = function() {
                            svClient.send('gridErase');
                        };
                    }
                };
            }
        ]
    );
})();