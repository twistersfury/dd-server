/**
 * Created by fenikkusu on 2/1/15.
 */
;(function() {
    var ng = angular.module('tf.dnd.directives');

    ng.directive(
        'tfImage',
        [
            'ngDialog',
            function(svDialog) {
                return {
                    template : '<span class="btn btn-info glyphicon glyphicon-picture form-control" ng-click="openDialog()"></span>',
                    replace : true,
                    link : function(oScope, oElement) {
                        oScope.openDialog = function() {
                            svDialog.open(
                                {
                                    template: '<div>Hello World!</div>',
                                    plain : true
                                }
                            );
                        };
                    }
                }
            }
        ]
    )
})();