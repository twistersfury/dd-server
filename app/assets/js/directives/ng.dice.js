/**
 * Created by fenikkusu on 1/26/15.
 */
;(function() {
    var ng = angular.module('tf.dnd.directives');

    ng.directive(
        'tfDice',
        [
            'tf.dnd.client',
            function(svClient) {
                return {
                    scope : {
                        tfDice : '='
                    },
                    link : function(oScope, oElement) {
                        oElement.addClass('tf-dice tf-dice-' + oScope.tfDice);

                        oElement.on('click', function() {
                            svClient.send('dice', oScope.tfDice);
                        });
                    }
                }
            }
        ]
    );
})();