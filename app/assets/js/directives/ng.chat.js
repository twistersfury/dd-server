/**
 * Created by fenikkusu on 1/31/15.
 */
;(function() {
    var ng = angular.module('tf.dnd.directives');

    ng.directive(
        'tfChat',
        [
            'tf.dnd.client',
            function(svClient) {
                return {
                    template : '<div class="row"></div><div class="col-xs-8"><input type="text" class="form-control" /></div><div class="col-xs-4"><button ng-click="send()" class="btn btn-primary glyphicon glyphicon-send form-control"></button></div><!--<div tf-image></div>--></div>',
                    link : function(oScope, oElement) {
                        oElement.addClass('tf-chat form-inline');

                        oScope.send = function() {
                            svClient.send('message', oElement.find('input').val());
                            oElement.find('input').val('');
                        };

                        oElement.find('input').on('keyup', function(oEvent) {
                            if (oEvent.keyCode === 13 || oEvent.which === 13) {
                                oScope.send();
                            }
                        });
                    }
                }
            }
        ]
    )
})();