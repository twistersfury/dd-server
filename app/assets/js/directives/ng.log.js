/**
 * Created by fenikkusu on 1/26/15.
 */
;(function() {
    var ng = angular.module('tf.dnd.directives');

    ng.directive(
        'tfLog',
        [
            'tf.dnd.client',
            function(svClient) {
                return {
                    link : function(oScope, oElement) {
                        oElement.addClass('tf-log');

                        svClient.on('message').then(
                            function(oData) {
                                var $liItem    = angular.element('<li></li>'),
                                    $spMessage = angular.element('<span></span>').html(oData.data),
                                    $spName    = null;

                                if (oData.player === null) {
                                    $spMessage.addClass('action');
                                } else {
                                    $spMessage.addClass('message');
                                    $spName = angular.element('<span></span>').addClass('user').html(oData.player)
                                }

                                $liItem.append($spName).append($spMessage);

                                oElement.append($liItem);

                                oElement.scrollTop(oElement[0].scrollHeight);
                            }
                        );
                    }
                };
            }
        ]
    );
})();