"use strict";

module.exports = function (grunt) {
    grunt.initConfig(
        {
            pkg     : grunt.file.readJSON('package.json'),
            compass : {
                dev : {
                    options : {
                        basePath : 'app/assets/css/_sass/',
                        config   : 'app/assets/css/_sass/config.rb',
                        force    : true
                    }
                },

                prod : {
                    options : {
                        basePath    : 'htdocs/<%= pkg.ident %>/wp-content/themes/<%=pkg.ident %>/assets/css/scss/',
                        config      : 'htdocs/<%= pkg.ident %>/wp-content/themes/<%=pkg.ident %>/>assets/css/scss/config.rb',
                        environment : 'production',
                        force       : true
                    }
                }
            },

            uglify : {
                js : {
                    files : [
                        {
                            expand : true,
                            cwd    : 'htdocs/<%= pkg.ident %>/wp-content/themes/<%=pkg.ident %>/assets/js/',      // Src matches are relative to this path.
                            src    : [
                                '**/*.js',
                                '!min/**'
                            ], // Actual pattern(s) to match.
                            dest   : 'htdocs/<%= pkg.ident %>/wp-content/themes/<%=pkg.ident %>/assets/js/min/',   // Destination path prefix.
                            rename : function (sDest, sPath) {
                                return sDest + sPath.replace('.js', '.min.js').replace('.min.min.js', '.min.js');
                            }
                        }
                    ]
                }
            },

            watch : {
                sass : {
                    files : ['app/assets/css/_sass/**/*.scss'],
                    tasks : ['compass:dev']
                },
                js   : {
                    files : [
                        'app/assets/js/**/*.js',
                        '!app/assets/js/min/**/'
                    ],
                    tasks : ['uglify:js']
                }
            }
        }
    );

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('run'    , ['compass:prod', 'uglify']);
    grunt.registerTask('js'     , ['uglify']);
    grunt.registerTask('default', 'watch');
};
